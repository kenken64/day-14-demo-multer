module.exports = function(app){

    var fs = require("fs");
    var path = require("path");

    var multer = require("multer");
    //var multipart = multer({dest: path.join(__dirname, "/upload_tmp/")});

    var storage = multer.diskStorage({
        destination: './uploads_tmp/',
        filename: function (req, file, cb) {
            cb(null, file.fieldname+Date.now() + '.jpg') //Appending .jpg
        }
    })

    var multipart = multer({ storage: storage });

    app.post("/upload", multipart.single("img-file"), function (req, res) {
        fs.readFile(req.file.path, function (err, data) {
            res.send(200);
        });
    });

}
